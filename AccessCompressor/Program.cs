﻿using System;
using System.IO;

namespace AccessCompressor
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                String accessPath = args[0];
                CompressAccess(accessPath);
            }
        }

        /**
         * Access数据库压缩方法
         * accessDbPath 为access数据库在磁盘的绝对路径
         */
        private static void CompressAccess(String accessDbPath)
        {

            if (!File.Exists(accessDbPath))
            {
                return;
            }

            string tempDbPth = "temp.mdb";

            tempDbPth = accessDbPath.Substring(0, accessDbPath.LastIndexOf("\\") + 1) + tempDbPth;

            string connectString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Jet OLEDB:Database Password={1}";

            string tempDbConnectiongString = string.Format(connectString, tempDbPth, "betterlife126126");

            string objectDbConnectiongString = string.Format(connectString, accessDbPath, "betterlife126126");

            try
            {
                JRO.JetEngineClass jetEngine = new JRO.JetEngineClass();

                jetEngine.CompactDatabase(objectDbConnectiongString, tempDbConnectiongString);

                File.Copy(tempDbPth, accessDbPath, true);

                File.Delete(tempDbPth);

            }catch (Exception)
            {
                if (File.Exists(tempDbPth))
                {
                    File.Delete(tempDbPth);
                }
            }
        }
    }
}
